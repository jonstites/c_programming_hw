/**********************************************************************
Author:      Jonathan Stites
Date:        6 June 2015
Class:       C Programming for Beginners
Instructor:  Bineet Sharma

Program:     lettergrader

Description:
This program takes an input file of students and grades 
and an output file as command-line arguments. It then 
writes each student's grade to the output file in 
alphabetical order of the students' names. It also
outputs statistics on the grades to stderr. 

Example usage:
./lettergrader.o input.txt output.txt

Input file has the following format:
Thui Bhu, 100, 90, 80, 100, 89, 99, 88
Ariana B. Smith, 90, 90, 100, 100, 99, 100, 95

Output has the following format:
Ariana B. Smith:    A
Thui Bhu:           A

Statistics to stderr:
Letter grade has been calculated for 8 students listed in input file input.txt and written to output file output.txt

Here are the class averages:
                Q1      Q2      Q3      Q4      MidI    MidII   Final

Average:        82.25   80.38   82.25   83.88   81.38   84.12   78.62
Minimum:        60      54      38      62      62      60      50
Maximum:        100     90      100     100     99      100     95


Press Enter key to continue...



***********************************************************************/



#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "utility_jonstites.c"



int main(int argc, char *argv[]) 
{

  // Number of students in the input file is unknown until the 
  // whole file is read
  int numStudents; 
  
  // Array of student structs.
  struct student allStudents[maxStudentNum];

  // Make sure files exist
  checkArgs(argc, argv);

  // Read input, add all students to the array in sorted order, and keep track of how many students.
  numStudents = collectAllStudentsSorted(argv[1], allStudents);

  // Write each student's letter grade to file, sorted alphabetically.p
  writeFile(argv[2], allStudents, numStudents);

  // Output statistics to stderr
  writeStats(allStudents, numStudents, argv[1], argv[2]);

  // Exit on Enter ("any key" doesn't seem to work on Linux, newline seems to be required)
  fprintf(stderr, "Press Enter key to continue...");
  getchar();

  return 0;
}



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Size limits
// Maximum characters in a line of input
#define maxLineSize     256

// Max number of students
#define maxStudentNum   100

// Max characters in student name
#define maxStudentName  100

// Maximum score conceivable on a test
#define maximumTestScoreEver 1000

// Minimum score conceivable on a test
#define minimumTestScoreEver -1000

// Comparison function, needed for qsort
typedef int (*compfn)(const void*, const void*);


// Struct of students for easy access to data
struct student {
  char name[maxStudentName];
  float mid1;
  float mid2;
  float quiz1;
  float quiz2;
  float quiz3;
  float quiz4;
  float final;
};

// Check command line arguments
int checkArgs(int argc, char *argv[])
{


  if(argc != 3) {
    fprintf(stderr, "Usage: commandname inputfile outputfile\n");
    exit(1);
  }

  if ((fopen(argv[1], "r")) == NULL) {
    fprintf(stderr, "cannot open input file %s\n", argv[1]);
    exit(1);
  }

  if ((fopen(argv[2], "w")) == NULL) {
    fprintf(stderr, "cannot open output file %s\n", argv[2]);
    exit(1);
  }

  return 0;
}

// Read one line from an input file
int getLine(char *line, FILE *InputFP)
{
  
  if (fgets(line, maxLineSize, InputFP) == NULL)
      return 0;

  if (line[strlen(line) - 1] != '\n')
    {
      fprintf(stderr, "Line does not fit in buffer: %s", line);
      exit(1);
    }
  return strlen(line);

}

// Takes student's name and array of scores, and adds those data to student struct
struct student studentAddValues(char *name, float scores[7])
{
  struct student thisStudent;
  strcpy(thisStudent.name, name);
  thisStudent.quiz1 = scores[0];
  thisStudent.quiz2 = scores[1];
  thisStudent.quiz3 = scores[2];
  thisStudent.quiz4 = scores[3];
  thisStudent.mid1  = scores[4];
  thisStudent.mid2  = scores[5];
  thisStudent.final = scores[6];
  return thisStudent;
}

// Take a line of input, and extract student name and array of scores
int parseLine(char *line, char *name, float *scores)
{
  char seps[] = ",";
  char* token;

  token = strtok (line, seps);
  getStudentName(token, name);
  getStudentScores(token, scores);
  return 0;
}

// Parse token for student name, copy into name array
int getStudentName(char *token, char *name)
{
  char seps[] = ",";
  if (strlen(token) > maxStudentName)
    {
      fprintf(stderr, "Name exceeds limit: %s", token);
      fprintf(stderr, "String length is %zd", strlen(token));
      exit(1);
    }
  strcpy(name, token);
  return 0;
}

// Parse token for student scores, copy to scores array
int getStudentScores(char *token, float *scores)
{
  int i = 0;
  float ret;
  char seps[] = ",";
  token = strtok (NULL, seps);
  while (token != NULL)
    {
      ret = strtol(token, NULL, 10);
      scores[i] = ret;
      token = strtok (NULL, seps);
      ++i;
    }
  if (i != 7)
    {
      fprintf(stderr, "Invalid number of scores in line");
      exit(1);
    }
  
  return 0;
}

// Take a line of input and return complete student struct with correct data
struct student studentFromLine (char *line) 
{
  struct student thisStudent;
  
  char name[maxStudentName];
  float scores[7];

  parseLine(line, name, scores);
  thisStudent = studentAddValues(name, scores);
  return thisStudent;
}

// Comparison function for student names
int compareStudents(struct student *first, struct student *second)
{
  return strcmp(first -> name, second -> name);
}

// Get student's final grade (not letter grade)
float gradeStudent(struct student thisStudent)
{
  float finalGrade;

  finalGrade = thisStudent.quiz1  * .10;
  finalGrade += thisStudent.quiz2 * .10;
  finalGrade += thisStudent.quiz3 * .10;
  finalGrade += thisStudent.quiz4 * .10;
  finalGrade += thisStudent.mid1  * .20;
  finalGrade += thisStudent.mid2  * .15;
  finalGrade += thisStudent.final * .25;
  return finalGrade;

}

// Get student's letter grade
char letterGradeStudent(struct student thisStudent)
{
  int finalGrade;

  finalGrade = gradeStudent(thisStudent);

  if(finalGrade >= 90)
    return 'A';
  else if(finalGrade >= 80)
    return 'B';
  else if(finalGrade >= 70)
    return 'C';
  else if(finalGrade >= 60)
    return 'D';
  else
    return 'F';
}

// Read input file, populate array with student structs, and return number of students
int readFile(char *inputFile, struct student *allStudents)
{
  int studentCounter = 0;
  char line[maxLineSize];
  FILE *inputFP = NULL;
  struct student thisStudent;
    
  inputFP = fopen(inputFile, "r");

  while (getLine(line, inputFP) != 0)
    {
      thisStudent = studentFromLine(line);
      allStudents[studentCounter] = thisStudent;
      ++studentCounter;
    }
  fclose(inputFP);
  return studentCounter;
}

// Read input file, populate array of student structs, sort array, and return number of students
int collectAllStudentsSorted(char *inputFile,  struct student *allStudents)
{
  int studentCounter;

  studentCounter = readFile(inputFile, allStudents);
  qsort(allStudents, studentCounter, sizeof(struct student), (compfn)compareStudents);
  return studentCounter;
}

// Populate array. Useful for debugging and for finding minimums and maximums
int fillArray(float *array, int arraySize, float filler)
{
  int i = 0;
  while(i < arraySize)
    {
      array[i] = filler;
      ++i;
    }
}

// Write student letter grades to output file
int writeFile(char *outFile, struct student *allStudents, int numStudents)
{
  int i = 0;
  FILE *outFP;
  struct student thisStudent;

  outFP = fopen(outFile, "w");
  for(i=0; i < numStudents; ++i)
    {
      thisStudent = allStudents[i];
      fprintf(outFP, "%-20s%c\n", strcat(thisStudent.name, ":"), letterGradeStudent(thisStudent));
    }
  
  fclose(outFP);

}

// Write statistics about scores to stderr
int writeStats(struct student *allStudents, int numStudents, char *inputFile, char *outputFile)
{
  int testNum = 7;
  float averages[testNum];
  float mins[testNum];
  float maxs[testNum];
  
  fillArray(averages, testNum, 0.0);

  // Initialize minimums and maximums
  fillArray(mins, testNum, maximumTestScoreEver);
  fillArray(maxs, testNum, minimumTestScoreEver);

  collectStats(allStudents, numStudents, averages, mins, maxs);

  fprintf(stderr, "Letter grade has been calculated for %d students listed in input file %s and written to output file %s\n\n", numStudents, inputFile, outputFile);
  
  fprintf(stderr, "Here are the class averages:\n");
  fprintf(stderr, "\t\tQ1\tQ2\tQ3\tQ4\tMidI\tMidII\tFinal\n\n");
    
  int i = 0;
  fprintf(stderr, "Average:\t");
  outputScores(averages, testNum, 2);
  fprintf(stderr, "Minimum:\t");
  outputScores(mins, testNum, 0);
  fprintf(stderr, "Maximum:\t");
  outputScores(maxs, testNum, 0);
  fprintf(stderr, "\n\n");

}

// Take array of scores and length of number of scores and output to stderr
int outputScores(float *scores, int scoresLength, int precision)
{
 int i = 0;
 while(i<scoresLength)
   {
     fprintf(stderr, "%.*f\t", precision, scores[i]);
     ++i;
   }
 fprintf(stderr, "\n");
 return 0;
}

// Collect statistics about the scores
int collectStats(struct student *allStudents, int numStudents, float *averages, float *mins, float *maxs)
{
  int i = 0;
  float grades[7];
  struct student thisStudent;

  while(i < numStudents)
    {
      thisStudent = allStudents[i];
      float grades[7] = {thisStudent.quiz1, thisStudent.quiz2, thisStudent.quiz3,
			 thisStudent.quiz4, thisStudent.mid1, thisStudent.mid2, 
			 thisStudent.final};
      int j = 0;
      while(j < 7)
	{
	  averages[j] += grades[j] / numStudents;
	  if(grades[j] < mins[j])
	    mins[j] = grades[j];
	  if(grades[j] > maxs[j])
	    maxs[j] = grades[j];
	  ++j;
	}
      ++i;
    }
  return 0;
}


